`Day 4 Session 3`

# UPGRADE, NOW! Jonathan Levin

http://NewOSXBook.com/msa

# Vulnerabilities

CVEs are cryptic. Little detail, not much score worth, potentially crazy serious bugs. Seemingly unending supply of them (thousands a year). Affect all systems, generally in proportion to code base size.

`http://www.cvedetails.com` - more Vulnerabilities _recorded_ is a good thing, more research. Look out for Code execution/injection vulnerabilities and privilege escalation.

CVEs are filed (only) when a vendor acknowledges vulnerability.

Use cases.. no, _abuse_ cases -- will turn out vulnerabilities.

# The \*OS Architecture

Mac/i/TV/Watch-OS share a huge code base
* user mode 60%
    - some frameworks (Mobile\*) reduced
    - major frameworks (Webkit..) exactly the same
    - Darwin layers virtually identical
* kernel mode 90% identical

## Apple's code base
- ASPL projects: Apple code, open source
- Proprietary: Apple code, closed source
- 3rd party: Misc open sources

Corollaries:
- bug in common code (mostly XNU) automatically compromise all \*OS
- bugs may originate in 3rd rate
- vulnerabilities are inevitable
- other-than-macOS are usually more secure than macOS, but enough so?

# Patch patch path

Often you just have to wait for Apple's patch.

10.10.5 is already EOL. So UPGRADE.

# Zero days

* Pure 0-day
    - hackers know, ROW oblivious
    - no remediation, no mitigation, no detection
    - exploit private
* Discovery: Technical 0-day
    - researchers know hackers may know
    - no remediation, mitigation and detection (likely) possible
    - exploit likely private
* Burnt
    - well known

# Patch patch... patch!

Time for some fun and real world examples

## 10.10

* ntpd
* rootpipe
* kextd
* DYLD_PRINT_TO_FILE
* DYLD_ROOT_PATH

Recommended blog `reverse.put.as`

macOS hardening thing: http://newosxbook.com/files/moxii3/macvuln/Appendix.pdf
