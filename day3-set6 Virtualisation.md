`Day 3 Session 6`

# macOS on ESXi and back - Rich Trouton

Building VMware Fusino VMs with Netboot

* DeployStudio and createOSXinstallPkg
    - stock copy or with additional spice
    - there's about 350 MB free space, so don't use more
    - pre- and post-installation scripts may fail
    - Create a DeployStudio image with Python included

`vfuse` by https://github.com/chilcote/vfuse

* First Boot Package Install Generator.app http://tinyurl.com/n2a8j2q
* AutoDMG -- Create-Recovery-Partition-Installer

# Building Fusion VMs without NetBoot

* Tim Sutton: Vagrant and Packer ++ http://git.io/2L4uag

# Building VM with Custom IS Installer Disk Images and System Management Tools

Create _fake_ os installer package

# Emulate specific Apple Models in VMware Fusion
* edit HW settings (VM must be shut down)
* open Config file in editor

lamw/ghettoV..duh

Read William Lam: VMXNET3 driver now included in MAC OS X 10.11 (El Cap)+.
That makes it a virtual 10G adapter!

* http://tinyurl.com/MSA2016VirtualizationPDF
* http://tinyurl.com/MSA2016VirtualizationKeynote

Also for virtuallyGhetto, change screen resolution "Workaround for changing Mac OS X VM display resolution"
