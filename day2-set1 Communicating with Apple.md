`Day 2 Session 1`

# File a radar

Yeah. Just do it. Even if it's a duplicate. Filed Radars will be taken care of. Unfiled probably won't.

`https://bugreporter.apple.com`

* If in documentation, there's a link at the bottom of the page to send feedback. That's the quickest way.
* Security vulnerability: the quick way is `mailto:productsecurity@apple.com`
* UI/UX

## Logs

### on macOS

* `sysdiagnose` (macOS)
* `serverloggather` (Server.app)
* `sample` applicationName
* Console (the new one)
* _Follow the instructions_ (31 of them...)

### on iOS

vol up - vol down - power (vibrate) does `sysdiagnose`. Connect to Mac and you can upload the log File

## Now what?

* Cannot reproduce / Insufficient information / Duplicate / Behaves correctly (as intended) / HW/SW/doc Changed / NMOS (?)

### tips

* Annotate screenshots
* Movies
* Be specific, explain acronyms
* Attention to detail
* Be professional

## And one more thing

`http://www.quickradar.com` by [Amy Worrall](https://github.com/amyworrall/QuickRadar)
