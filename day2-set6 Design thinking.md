`Day 2 Set 6`

# Design thinking for Mac Admins

_All experiences are designed. Not all interactions are designed to be easy to use._

Emotion > cognition

Design matters and we all design
* If you touch it, you design it
* Chances are you're already a designer!
* _There are no non-design choices_

Cat .gifs are relevant, but that's not all what design is

* Discoverability
* Feedback
* ..?

**Good design is all about empathy, making good assumptions**

# What is design thinking?

* A human centred approach to problem solving and innovation
* observe people behaviour, how the context affects interactions

0. who are you designing for? what do you want to create?
    - don't limit yourself to a pre-determined outcome
1. empathy (discovery)
    - garner a better understanding of the issue/current state through the eyes of the customer
    - human-centered focus
    - observe, engage, immerse
    - outcome: a customer experience blueprint of current state
    - example: adding a printer
2. interpretation (defining)
    - goal: refine issue, make it clear and robust and meaningful
    - how: search for emergent terms/patterns
    - outcome: turn discovery into understanding needs
3. ideate
    - turn interpreted insights into ideas
    - divergent thinking
        - brainstorm, suspend judgement, joo ja
    - convergent thinking
        - refine ideas
    - outcome: generate lots of ideas, choose a few to move forward
4. prototype
    - bring ideas to life!
    - build, map, wireframe
5. test and evolve
    - put it in the hands of users
    - gather insight: observe and ask users
    - refine, repeat

# Focus on the experience, not (necessarily) on the how (yet)

* You can't know your user's experience
* Design thinking != Rocket science (but rocket science = design thinking)
* Walk around the office at random times and observe what the users are doing (and how)
* Hold New Hour Office Hours whenever new employees start working at the company. Note what they ask about and see if you can incorporate it into your orientation talk.
* When someone comes by for help or sits in a meeting playing round on their computer...
* Design _for the future you_
* Make documentation accessible
    - make it accessible
* Talk to new hires and customers
* Work with volunteers
* Co-create
* Ask for feedback in Munki
* Find a way you like for processing feedback

* Good design has discoverability, feedback and is reflective (make mental models)
