`Day 3 Session 5`

# iOS Säkerhet - Nina Z Kominak of Apple

## Secure by design

* System
    - integrated hardware, software, services
    - secure startup, activation, updates
    - device protection and authorization
* Data
    - At rest and in transit
* Apps
    - Platform and ecosystem

* Android bashing:
    - OS and device market fragmentation (24k unique Android devices)
    - most malware targets Android, not iOS

* Secure Enclave processor
    - protects passcode and fingerprint data
    - provides key management for data protection
    - separate processor isolated from iOS
    - no known attacks on Secure Enclave reported (BlackHat 2016)... yet
    - Dedicated SoC core - trusted environment for crypto material
    - Arbitrates all user data accecss
    - Hardware accelerators fot AES, EC, SHA
    - Manages its own encrypted memory, communicates with Applicatino processor through a specific Mailbox
    - Factory-paired secure channels to TouchID sensor and Secure Element

## Secure boot

* Hardware root of trust
* Random, cryptographically validated at startup
    - boot ROM
    - Low-level Bootloader
    - iBoot
    - iOS kernel
* DFU mode enables return to trusted state ("connect me to iTunes")

## System software authorization
    - Activation server uses Device ID to "personalize" authorisation per device
    - Prevents downgrading to exploit older versions of iOS
    - Ensures system software is exactly as provided by Apple (signed)

## Device protection

* Complex passcodes with policy enforcement
* Time delays protect against brute force
* Local wipe after multiple failed attempts
* Delays enforced by secure Enclave on devices with an A7 or later A-series processor

## Touch ID

* Enables longer, more complex passcodes
* Sensor only active when Home button detects finger
* Framework for third-party apps
* Apps can prevent authentication when fingerprint is added or removed
* Fingerprint is not stored, hash is

## Data security

* Protect data at rest and in transit
* File-based data protection
* Industry standard secure networking (?)

- User data protected by strong cryptographic master key derived from user passcode and device ID
- No offline attack on user passcode; hardware-based master key
- No brute force limit on number of attempts
- Hardware keys for master key derivation not exposed directly to any mutable software
- Secure support for alternative unlock mechanisms (eg TouchID)

## Data protection classes

- Compleat protection - Class A (256 bit AES), eg health data
- Protected unless open - Class B (Curve 25519), eg download of mail
- Protected until first user authentication - Class C (256 bit AES), default class (for development)
- Protected only with UID - Class D (256 bit AES)

## keys

Keychain:
* in SQLite3 database
* `securityd` controls access
* Items shared *by the same developer*
* Keychain data protected based on the file protection classes

Keybag:
* Management of keychain and file protection classes
* Collection of class keys
* Keybags for various purposes
    - collecting and managing keys for file and keychain Data Protection classes: user, device, backup, escrow, iCloud backup

## iCloud backup
* Device settings, app data (but not apps), Camera roll, iMessage conversations
* Only secure transport to iCloud (so https, not http)
* Backups taken when locked, connected to power, and on WiFi
* Files are backed up in their original, encrypted state
* iCloud password is not related to encrypted backup

## App security

* Verify identity/authenticity of all developers
    - dev must be registered w Apple (including credit card = 99$)
    - verified real world identity
    - app store review process
* Apps go through an extensive review process
* Enables safe (restricted) communication between apps
* Provides secure authentication models

## Code signing

* Mandatory application signing ($299 for internal distribution!)
* Ensure app integrity and authenticity
* Verified during app launch and runtime
* **DO NOT CLICK REVOKE IN XCODE.**
* **JUST DON'T.**
* Revoking your internal certificate is really bad.

## Runtime process security

* App sandboxing
* Entitlement declarations
* Data execution prevention
* Anti-virus will probably operate only in its own sandbox

## Entitlements

...allows a microphone to plug to Evernote (or the other way around, if you want to think it so)

## Enhanced security for in-house apps

* ...require users to trust the developer
* Apps installed with MDM do not prompt user for trust confirmation
* IT can disable app installations from unknown developers
* Users can see all enterprise apps installed

## Separation

...between personal and corporate data

* Seamless for the user
* Personal documents go only to personal apps, corporate docs go only to managed apps

## Privacy

* Built in on iOS
* Greater transparency for the user

## ..and MDM

* MDM only sees corporate data
* Users' personal data outside

## MDM can see

* Device name
* Phone Number
* Serial number
* Installed apps

## MDM cannot see

* Personal email...

# What's new on iOS security

* Apple Transport Security enforced at the end of 2016
* Exceptions require justification
* Goal to enforce the developers to use secure protocols by default

# Deprecated algorithms

* RC4; SSLv3 in securetransport
* Going SHA1, 3DES

# Differential Privacy

     * Collecting user/usage behaviour
     * Use behaviour of large number of users
     * Add mathematical noise to a small sample pattern - more patterns, generalizations can be made

# And one more thing

Security bounty program (invitation only)


Q? Can Secure Enclave be updated?
