`Day 1 Session 3`

# Practical Security - Edward Marczak

`goo.gl/uh,something`

What can _you_ do?

## The Basics: Apple Products

* Responsible disclosure vs Zero Day
* Threat model
    - the Meteorite model
    - if you're a .gov agency, you've already lost
    - ...should be realistic (so skip the Meteorite model)
* Apple
    - Thunderstrike attack, using Thunderbolt vuln, overwrote firmware
* All technology
    - your WiFi
    - your ISP
    - ...their technology
    - Whatever is connected to your network
* General nosybodies
* Losing a device
* Competitors
* Government
    - yours, or foreign
    - if you're a target of a foreign government, give up
    - travel with minimal data
    - chromeOS, just an iPad (not your main iPad)
* Protecting data
    - if you're not an expert, pay one (or become one!)
* Convince everybody that security is everyone's job
* **Security requires empathy**
    - for your end users
    - when you're on a time crunch, you make the worst decisions
    - the tech industry fails by putting the security burden on the end user

### Passwords
* A longer, more memorable password trumps a cryptic one
* Forcing people to change their password doesn't improve security
* Change all default passwords
    - krebs on security ddos was done with default password IoT things
    - shoden search engine (VNC is fun)
* Two factor authentication
    - twofactorauth.org

### Patching
* SUS and pray
* Casper and Munki
    - Munki force install updates by date
    - Munki updates all software
    - Users will avoid a reboot
    - Less so: Puppet and Chef

### Encryption
* Filevault
    - push out a profile where it'll send your key (this sucks)
    - rather: Cauliflower vest, or crypt2

### Anti virus
* Depends on your threat model
    - and your users, if they are vulnerable to low hanging fruit
    - if you are working with Windows users

### Gatekeeper and firwall
* Enable it
* Little Snitch
    - also in monitoring mode
    - install plain Sierra and Little Snitch, then feel bad about the amount of traffic

# The Intermediate

* Stop buying shit
    - snake oil that the vendors say that solve everything
    - _We prevent cyberattacks_ ... no, you don't
* Inventory
    - know what's on your network
    - what devices _should_ be accessing stuff on your network
    - BYOD requires company management profile
* Logs and dashboards

# The advanced
* Quarantine and Incident Response (IR)
    - malware happens
    - ...or just unwanted stuff
    - how do you find and stop it? seek and destroy?
    - IR: wipe the machine?
    - splunk (has a free tier but it can get expensive)
    - ELK stack
* Binary whitelisting
    - SANTA from Google (kernel extension)
        - monitor only -mode
        - blacklist only -mode will stop given binaries like MacKeeper
        - fully protected mode: vote if a binary is kosher, makes people _think_
        - zentral server
        - google SANTA server will be open sourced rsn


# Takeaways?

* Patch patch path quickly
* A secure Internet is better for everyone
