`Day 1 Session 2`

# From Shell to Swift, Joel Rennich

* _Stupidly_ easy to get into
* Lots of API and community support
* Apple's future for the next few years

Cons:

* Can be _frustratingly_ picky
* Compiled but needs a runtime, minimum app is 6 MB
* "Dynamic", "Still young" (so changing, _"your coding language is pretty solid"_)
* Comes from iOS

Swift 3 is not Swift 2
* Less script-like, more like Objective C

## For administrators

* Compiled, so it can be easily signed
* Access to system APIs; you don't have to rely that the output of `ping` doesn't change...
* Not easily human readable (?)
* Some things are easier with Bash

Get `The Swift Programming Language` iBook

## Some Swift tips

1. Only read the first third of the Swift language
2. Take an existing Bash project and convert it into Swift
3. Use Playgrounds, but not too much :)
4. Be prepared for a little bit of disappointment

## How Swift will annoy you

* Asynchronousicity - your code will no longer be linear
* The deeper you go, the more low-level you'll go
* Really, really, really type safe
* Pointers (?!) and memoty management
* Having to go back to ObjC and shell
* You may be the first person to ask a particular Swift question (which isn't about Taylor Swift)

## Some examples

1. How to use Playgrounds

`Hello, Playground`

2. Fun with `Process()` (was: `NSTask()`)

`http://www.gitlab.com/mactrolls/swiftbits`

3. Creating a project

4. What i() am using Swift for

* Casper user setup
    - wrapper around `QuickAdd.pkg`
    - like DEP
    - get a GUI for a script
* `NoMAD`
    - everything about AD without the bind
    - SSO, password expiration warnings, password changes...
    - manageable with profiles/MDM
    - #nomad on macadmins slack
    
