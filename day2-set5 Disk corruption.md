`Day 2 Session 5`

# Disk corruption and SoftRAID

* Learn from experiences from others
    - practice, practice, practice until it becomes automatic
* Don't assume it won't happen to you
* Protect yourself from disaster

Backblaze (google `backblaze disk failure`) has stats on disk drive reliability
* Older Seagate usually sucks, newer is good
* Toshiba is good
* WD is not very good
* HGST is exceptional

Disk drive failure rate is U-shaped, minimum at 6 months-ish. Test ("certify") your drives before you start using them.

## SMART

* SMART Test fails to report that they're broken (usually)
* SMART Attributes are not entirely useless (three out of 80 parameters are genuinely useful) :

| Attr  | Meaning                       |
| ----- | ----------------------------- |
| 5     | number of reallocated sectors |
| 197   | unreliable sector count       |
| 198   | uncorrectable error count     |

* All values must be zero!
* 3/4 of predicted failures do happen

# SSD Failure Rates

Writing only after erasing, which is a "violent operation". 3000-5000 writes until a single cell is worn out. All SSDs come with extra space which is reallocated. At some point you run out of the extra space, after which the disk will be read only. HFS+ will have a problem mounting a R/O disk.

Look for the Media Wearout Indicator, 100%-->0%

But SSDs fail before MWI goes to zero. There is no reliable way of predicting failure. They are dying for other reasons. SSD failure correlates with age.

# Worst case Scenarios

* Events that happen to 1 in 1k-10k users.
* We don't think it'll happen to us
* When they do, they can destroy our business

## Case Cirina Catania

* She was properly prepared with multiple copies of everything.
* The disks had been repartitioned into 18 zero byte partitions...
* Professionals got 20% back from the best volume
* Her backup and only source were on-line at the same time

## Case Ryan Francis

* Certified his old RAID rather than the new one
* They had no backups (at that stage)
* SoftRAID now has safeguards that you can't certify a disk that marked is in use

# Do This

* Have 2 offsite backups of all critical data
* Alternate which backup you use
And two more

* Have a fire drill for your data
    - imagine your computer and all disks disappear
    - or that your office burns down
    - do you have all important work stored offsite
    - how long would it take to restore it to a new computer
    - what happens if you lose your computer and all disks during backup

* Roll those backup disks more often than every 30 days

* Replace an SSD drive after 5000 hours on laptop, 10k hours on workstation and 20k hours on server
* Disks are cheaper than the data you lose on it
