`Day 1 Session 1`

MacSysAdmin 3-6 Oct 2017

# Administrators' State of the Union - Kevin White - MacJutsu

Apple already does a good job on streaming their keynotes, so this is the angle.
Apple doesn't talk a lot about us.

The AirDrop thing (that failed?)

* ConnectED case study
* iOS 9.3 and the updates thing
* Apple .edu customers
* iOS 10
* macOS Sierra
* iCloud
* Network & Security
* Device management
* macOS Server 5.2
* Future technologies

## ConnectED Case

ConnectED is an Apple brand ($100M grant to underserved schools in USA). Case: zero touch iPad deployment. Import scripts for JSS (if that's your poison). Significant infrastructure updaes (wifi). 4500 teachers and staff MBPs & 40'000 student iPads, 350+ days of consultancy.
www.apple.com/teched

**Zero touch** architected by Eric Williams (Apple SE). Used Casper Suite, Charge and sync station or cart. DEP iPads. And Asset & student information to _assign_ the iPad to the right individual (assign individual to iPad). Asset tags. **Apple Configurator 2** used.

Gotcha: using Configurator, remove the original iLife apps and replace them with the VPP versions of the same software.

## iOS 9.3

...came out in the spring.
### Profile additions
* EDU configuration for ASM
* Restrict apps (whitelist/blacklist)
* Manage home screen layout
* Restrict notifications (down to per-app)
* Restrict Apple music services (down to naughty lyrics)
* Restrict developer diagnostics
* Managed domain password policy

## Lost mode requirements
_It's not lost mode if someone has to open up an app_

* Find my iThing = iCloud, not MDM
* AppleID not required
* Cellular/GPS hardware not required
* Location services not required -- lost mode will turn it on!
* Locks device until MDM unlocks it and puts it in low power mode
* Location updates require MDM push
* User is notified after unlock (you can't stalk your user unnoticed)

## Lost mode something

## Apple EDU technologies

You don't _have_ to be an EDU. Training, classroom application.

* Apple school manager
* Managed Apple IDs
* Shared iPads (finally -- has multiple users for iPad)

### Apple Classroom app

Requirements
* Wifi with client to client communication
* MDM that supports _Classroom_
* Apple school manager not required
* Instructor cannot modify class roster
* Instructor: managed (not necessarily supervised) iOS 9.3+, iPad 3 or mini, or later
* Student: supervised & managed iOS 9.3+ with AirDisplay to clasroom app (iPad air 2, mini 4 or later)
* Wifi and Bluetooth on
* Clasroom app
* Configuration deployed via EDU profile (not necessarily over MDM)
* Instructor can observe iPads. Students will know they're being watched (blue bar)
* Instructor can open & lock to an item (apps, books, websites, iTunes content)
* Instructor can unlock & clear password and log out user from multi user iPad;
* start/request AirPlay session, requires managed AppleTV
* http://school.apple.com (requires K-12 license)

Apple school manager (ASM):
* Replaces EDU DEP
* Eventually replaves EDU VPP
* Available in any geo that has DEP (!)
* Compete against Google and Microsoft
* Intermediate directory service for MDMs (that you have to manage, which _is a pretty big deal_)
* Only methid for Managed Apple ID:s and for Shared iPads

Manage ASM users
* Users identified by email (eventually AppleID)
* ASM roles: administrators and managers
* EDU roles: staff, instructor, student
* Class roster = groups, with metadata like room and name
* Can be made manually in MDM web site, _but_ you probably want to import your data with ASM Setup Assistant (which you must enable from the start)
  - can import users & class rostes
* SFTP upload to Apple, properly formatted CSV:s
  - you can script this
  - scrape your directory, send it to Apple
  - Apple creates an sftp account for you
* Your MDM syncs back from ASM nightly
* MDMs can consolidate between MDM and LDAP (which can become very tricky -- to get the fields right, so be picky about your MDM!)


Managed Apple IDs

Shared iPads - single user device with multiple users
* MDM that supports Shared iPads
* Supervised and managed iOS 9.3+
* Wipe and reset so re-enroll via DEP and Shared iPad enabled
* iPad Air 2. iPad mini 4, or later
* Minimum 32 GB storage (depending on number of users per device)
* You predetermine how many users an iPad gets, then each user gets their own partition
* Older users data is deleted
* Heavily leverages cloud services, everyone saves everything on cloud (not necessarily Apple's)
* Also requires a bunch of network capacity
* Managed AppleIDs are _strongly suggested_ (for upload and download to the cloud)
* Apple Caching Service is _strongly suggested_

## iOS 10

* HW: iPhone 5 or later, iPad 4, mini 2, later, iPod Touch
* WiFi, Bluetooth

* SiriKit limited by intents domains, for now
  - Apple hasn't defined all kinds of intents (like Tindr)
* Maps and Messages integration for 3rd party apps
* Improved notifications for 3rd party apps
* CallKit could be used by VoIP vendors; huge ramifications

## Swift Playgrounds

* iOS, free, amazing, for tweens and later :)
* First attempt at _compiled code_ on iOS
* Special Swift coding keyboard (!)

# macOS Sierra

* HW: Late 2009/2010
  - unlock with Apple Watch

## Features _removed_ on Sierra

* AppleID as login password (?)
* Portable home directories
* GUI "Allow all" in Gatekeeper
* Kernel panic core dumps (enabled in macOS Recovery)
* Older than v5 XSan
* https://support.apple.com/en-us/HT206871

## Tabs

* Any application can have tabbed windows -- merge all windows
* Dock preference / Prefer tabls when opening documents: "Always" - fun ensues

## Gatekeeper

* Fundamentally the same thing, they removed one button though (that last option)
  - you can put it back on the command line
* Unified logging! Single system to coordinate _all_ logs (regardless of iOS, watchOS, macOS)
  - Update command line: `log`
  - Can view logs from connected devices
  - updated console application
* Advanced search still there
* Can now search for related messages (in other logs!)

## Storage manager

* In System information
  - open via About This Mac
  - Reduce local storage usage
  - integration with iCloud (and hopefully other third party providers)

## iCloud Drive App

* Optional installed iOS App, otherwise users won't see "All your files in iCloud"

## iCloud Desktop and Documents

* Optionally sync Desktop and Documents
* Once enabled, iCloud is authorative
* Items are **MOVED** in and out of iCloud
* Finder preferences: Warning before removing
* New Macs only download data when needed
* Old Macs may remove download to save space (on local computer)
* Old Macs added will create sub-folders -- for each physical Mac you log in to (ungh)

## Real-time collaboration

* Add People, shared files in iWork and Notes app
* iOS 10+, macOS Sierra
* Beta

# Network and Security

## Wireless standards

* Apple partnered with Cisco
* Zero client-side Configuration
* 802.11 ac Wave 1 and 2, k, r, u, v
* PPTP VPN removed
* TLS default, SSLv3 and RC4 deprecated
* Deprecated: macOS: non-AES Kerberos deprecated (old AD, Server 2008-ish)
* macOS: SMB requires signing by default
* macOS: AES encryption for Kerberized NDS

## ISO 27001 Certification
* iCloud* iMessage and FaceTime
* iTunes U
* ASM

## Device management
* iOS 10 Profile additions
* Personal firewall management
* IKEv2 VPN payload improvements
* Granular iCloud service restrictions (like restrict keychain!)

## Setup assistant management
* MDM can keep device in Setup assistant
* macOS hidden: iCloud Desktop, Siri
* macOS: 802.1X during setup assistant

# Server 5.2

Time Machine now supports SMB shares

* SUS DEPRECIATED
* SUS still manageable via View manually
* SUS removed in next release
* Perkele
* Profile manager - Full support for ASM
* Caching service improved granularity

# Future technologies

* APFS
  - optimised for Flash storage
  - clones, snapshots
  - single and multi key encryption
  - copy on write for metadata (bye journalling)
  - atomic safe-save for bundles and directories

* AFPS only for testing in disk images right now
* Must migrate from AFP to APFS (AFP related to HFS)
* Must migrate from FileVault 2 to some new encryption system
* New mechs for Fusion Drive
* New Time Machine mechanisms (cloning and snapshots)
* Third party drive support, software RAIDs, need to be re-written
* Potential "iOS-ification" of macOS file system

* **Thunderbolt 3*** One Plug to Rule Them All
* Apple CPUs in Macs, come A10
  - Apple prefers to control primary technologies
  - Touch ID embedded in CPU, ont supported by Intel
  - waiting for the new Intel schedule...
  - Apple performance out-perform Intel?
  - macOS Sierra code for "ARM Hurricane"
  - `/System/Library/Extensions/AppleHV.kext` -- hypervisor!

 ## For more information
 * https://help.apple.com/deployment/macos
