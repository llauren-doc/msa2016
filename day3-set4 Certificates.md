`Day 3 Session 4`

# Certificates... how do they work? Marko Jung, _Galactic Viceroy_

There's an AppleTV app for MacSysadmin!11# (by Waldman Media)

## Cryptography

* Symmetric keys
    - problem: seure key exchange (is not always practical)
    - problem: does not scale
* Asymmetric keys,

* Public and private keys
    - key is private key
    - lock is the public key
    - encryption and decryption are one way operations
    - decryption performs the inverse of encryption

* Secure cryptographic key exchange using DH
    - James H Ellis from GCHQ

* Example with colours

* In Cryptography, a certificate is to prove you own a private key

## Keychains

So many Keychains
* Login keychain, specific to user, locked with login password (usually)
* iClould Keychains or Local Items: user specific
* System keychain, not user specific authentication assets; only admin users can make changes to it
* System roots: system wide trusted root Certificates

Browsers have different Root Certificate stores.

You just don't walk into the Root Certificate Store.
(www.apple.com/certificateauthority/ca_program.html) and it can cost a lot.

## Public Key Infrastructure

* A Certificate Authority (CA)
* Registration Authority (RA)
* a third-party Validation Authority (VA)
* A directory (secure location to store and index keys)
* Certificate Management System
* A Certificate Policy

### Certificate Revocation Lists

```
% openssl crl -in apple-ca-root.crl -inform DER -text
% openssl crl -in apple-wwdrca.crl -inform DER -text # developer keys
```

2.45 million revoked certs..

### OSCP

## PKI Implementations

* Active directory
* OpenSSL
* EJBCA
* OpenCA PKI Research labs
* JAMF and a lot of other MDMs

# Let's Encrypt!

Google: Incident WOsign :D
COMODO free mail SMIME certificate

# Certificate Transparency?

* Open framework for monitoring and auditing of SSL Certificates
    - detect SSL Certificates that have been mistakenly issued by a CA or maliciously acquired from an otherwise unimpeachable CA

# SCEP "Simple" Certificate Enrolment Protocol

FYI: http://github.com/mjung/publications

Special Effect: http://mju.ng/give
