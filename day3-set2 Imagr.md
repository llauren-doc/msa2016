`Day 3 Session 2`

# Imagr - Graham Gilbert

Open source, runs on Linux (if needed -- only needs web server)

You can run Imagr on its own

To create a netboot/imagr image, you can use [nbicreator](https://github.com/NBICreator)

* Create a text `imagr_config.plist`
...

Investigate:
* [`ClearReg.pgk`](https://github.com/grahamgilbert/macscripts/tree/master/ClearReg) which skips setup assistant (nice on first boot)
* Create_msaadmin
* [outset](https://github.com/chilcote/outset)

? wipe PRAM on imaging?

* `github.com/grahamgilbert/imagr_macsysadmin_2016`
* `github.com/grahamgilbert/imagr`
