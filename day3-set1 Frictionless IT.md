`Day 3 Session 1`

# Frictionless IT (Zendesk)

Great IT is not (only) about what you do. IT is about how people _feel_ about what you do.

The Service desk is not to be treated as a cost center. Make support a _positive experience_ -- a brand. Experience > transaction.

Trust that IT is rock solid about its delivery.

Garner hypes: Customer experience + _sustainable_ delivery

Bimodal, two modes of working:
* mode 1 is predictable: take the order from the customer and devliver
* mode 2 is exploratory: explore possibilities hand in hand with our partners ("license to drive")

(Can you provide SaaS as a service :D )

# Process

* light
* just enough structure
* give a process to smart people will make them dumber, trying to game the process
* if something sucks, you want to know about it
* process = enabling scale
* distributed throughout the world
* Minimum viable process!

## What stuff are you doing?

* Tier 1 and 2: break-fix and genera lhelp
* Small projects (hang a radiator in the hall)
* operationalizing new capabilities from Engineering
* compliance audits and fixes
* security

**Think of past, present and future**

The Work Portfolio : A lot of 1+2, small slice of fixing past mistakes (tech debt); making things incrementally better (through automation); thinking strategy +2 years so we can be  a strategic differentiator.

## Agile is good for help desks tools

Some ideas come from outside IT, like Agile. Agile is simple, about principe, collaboration, MVP, pushing decisions down, ppl closest to the work make choises how implementation, no duplication of efforts. What we tackle is reviewed every 2 weeks on review time. Great for transparency. Ppl need faith that IT Delivers. Become _predictable_ which is part of our brand as a great help desk.

## Light: Just Enough Structure (and ITIL)

* Asset management
* Vendor management
* Incident management
* Change management
* Access management
* compliance

Work backwards from the metric you want to capture. How much is human error vs system error? Fix and re-evaluate Incident management.

## Your IT brand

* All channels are good
    - Ppl come from anxiety - don't make it hard for folks to tell you about it
* First impressions matter
    - give a fully installed clean computer to new hires
* Configure where you can, code where you must
    - SaaS: : roll out features by configuration
    - Safer to do so, less time in break-fix, more time rolling out capabilities
    - code has a lifespan/life cycle, also more fun :D
* Go higher touch (?) when needed (VIPs)
    - not every ticket is equal
    - you have to satisfy everybody; there will be noise so be responsive to VIPs (title? thought leaders?)
* "If you see something, say something"
    - being proactive, training
    - record so you can fix later
    - reboot equipment before they start to suck
    - monitoring
    - best scenario: manage the incident nobody knew was coming
* Communicate early when there is a problem
    - bad news does not get better with age
    - ppl will trust that no news means no problem

## Metrics

How do you measure the calue of your service desk? Be cautious of wrong metrics, they create bad

* Only measure things you _can measure_
* only things that will enable you to take action
* be cautious of unintended consequences
    - measure closed tickets and ppl will just work on closing easy tickets

## Device management and corporate culture

* Make friends
    - engineers will be suspicious about your snooping
    - porn and bittorrent discovery (be nice)
* Pilot
    - have friendly ppl Pilot
* Don't assume corporate communication mechanisms still work -- DIY
    - if you want to do it, get yourself known and on the agenda
    - travel, do q and a
* Show and tell
    - X-Files
    - This is the info we collect, this is what we can change, etc


## The Who? People

What are you really hiring _for_ -- quirky ppl, empathy, not only tech. _Friendly badass geeks_ - Customer Service DNA. Possible to teach any skill; tech changes so fast. Shop for the things you can't teach, like empathy, humour.

_We hire Zendesk employees, not service desk specialists._

You are preparing ppl for a career in the company, you are the top of the funnel.

Fun fact: Zendesk IT is currently 26% female. Still, don't do a Diversity Hire. Don't hire somebody for their body parts. Hypothesis: strict about only hiring _collaborative_ ppl. Makes supportive environment. Consistent about fighting work, not each other. Comes through in the interview. Means somebody "in minority" will end up in the hiring squad.

## Set the tone

* _It's not what you said, it's how you said it_
* Start with "Yes"
    - assume request is reasonable
* Don't be afraid to go off the menu
* Talk you your customers like they're people
* You are an experience, not a transaction
* Humour is Great
    - eg, Zendesk introduced vending machines, including glitter tattoos
    - t-shirt "Zendesk IT, we solve more problems than we create"

## technology

Help your helpdesk deliver great Service

Wall to wall cloud, architecture approach. SaaS makes upgrade discussions go away. But not all SaaS is equal. Deletaged administration. SAML. Stuff.

If not SaaS, then PaaS or IaaS. Last resort: On Prem.

_"We can lose a building and nobody will know"_

UI matters!
    - some GUIs show their age

## Automate for scale

* IT's called "IT" for a reason
    - incremental progress is still progress
* Go for a single pane of glass
    - information integration
    - bring in info into where they work
* Eliminate any google doc where you keep data
    - red flag, we haven't got our master data strategy down
    - umntsa?
* Trigger human actions with workflows based on data changes
    - new hire? laptop ready for refresh? gear loaned? (and not returned?)

## Self service and ticket deflection

## Set smart standards

* Mostly Macs, Windows is hard to kill
* Control the SSO portal for app discovery - SAML is your friend
* Be cautious about "Enterprise Class" for _anything_ that touches the revenue chain
* Eventually: get Legal, Security and Procurement involved
* Anyone with a credit card is in IT - make sure the herd is culled :D
