`Day 3 Session 3`

# Scripting Opportunities for System Administrators - Greg Neagle

# Why?

What am i trying to accomplish?
* Init?
* System management
* Settings, preferences
    - if you can, use a configuration profile
    - if you must, use a script
* Fixes/workarounds
    - disable adobe creative cloud icon
* Automation/admin tools

# When?

* On demand
* When building the machine
* When installing sw (some sw can only be installed w a script)
    - or post install, like set license server or munki/puppet server
* On a repeating schedule
    - munki, Apple SUS...
* At startup
* login
* Logout

# Why + when

Why: System configuration \\
When: First boot (or rather, Every boot)

Why: System management \\
When: Every boot/peroidically

User settings: Login

# Who?

`root`, current user, all users?

# How?

Packages - can contain scripts, run scripts pre, post, at; also payload-free packages that only run scripts

```
% pkgbuild --nopayload --scripts Scripts --identifier "se.macsys.bluetooth"
 --version 1.0 TurnBluetoothOff.pkgbuild
 ```

Munki!

## login/logouthooks

- run as `root`.
- eg. Filevault assistant, cleanup
- have been deprecated since 10.4-ish
- only place for one of each, so run it as master to run other scripts

## launchd

Your Number One Goto Process

LaunchDaemons
- as root
- `/Library/LaunchDaemons`
- loads at startup
- Example: run LaunchDaemons at startup
- Repeating scripts, like cron.
- allow a non-admin to run a script as `root`
    - example: touch a file means that it runs a script (app must remove script afterwards!)
_I can't do the math... I'm an American_ (Greg)

LauchAgents
- as users
- `/Library/LaunchAgents/`, `~/Library/LaunchAgents` (try not to use that one)
- run a script at login
- example:
- launch a process at login window(!)

## common usage

* run a script at startup
    - Just use [outset](https://github.com/chilcote/outset)

* run a script at user login
    - loginhooks (deprecated)
    - there is only one
    - runs as root

* LaunchAgents
    - runs as user
    - ..after user login is complete (may take a few seconds or more)

* loginwindow authorization plugin: runs during login process as root
    - not exactly admin friendly...
    - Per Olofsson (@magervalp) created framework for this: [`LoginScriptPlugin`](https://github.com/MagerValp/LoginScriptPlugin)

* crankd
    - Chris Adams (acdha), Nigel Kersten
    - pymacadmin `https://github.com/MacSysadmin/pymacadmin`
    - react to application launch/exit/activate, volume mount/unmount, wake/sleep/powering-off...
    - grahamgilbert: using crankd to react to network events (blog post)
    - clayclaviness: github (github.com/google/macops/tree/crankd), to keep track on how often apps are started (for licensing)

_Spinning Rainbow of Patience_
