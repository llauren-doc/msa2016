`Day 4 Session 2`

# Command line tools for macOS - Ed Marczak and Greg Neagle

Book: Enterprise Mac - Managed preferences

Slide notes: https://goo.gl/LrAQmb

https://docs.google.com/document/d/1RixWx7Z7eos4PnxPBoopl6DPWc_yC9uTFbFcdceAeCU/edit?usp=sharing

`bash` is now available on OSX, Linux, BSD and Windows. `PowerShell` is also available on them all (and is also open source!). Bash is even available on Automator.

# pkgutil

Query and manipulate macOS installer packages and receipts. Determine if it's installed.

```
pkgutil --pkg-info com.adobe.pkg.FlashPlayer
pkgutil --expand BlueJeans...pkg /tmp/bj
```

# pkgbuild

Build a macOS installer component package from on-disk files

Task: install one file: `/Library/Security/PolicyBanner.rtf`

```
mkdir -p ~/git/admin/PolicyBanner/payload/Library/Security/
cp PolicyBanner.rtf  ~/git/admin/PolicyBanner/payload/Library/Security/
pkgbuild --root ~/git/admin/PolicyBanner/payload ...
pkgutil --payload-files PolicyBanner.pkg
```

Use pkgbuild to _install_ an app

```
pkgbuild --component /Vol/Firefix/Firefix.app \
    --install-location /Apps \ ...
```

# installer

Well guess what :D

# dscl

Directory service command line utility

```
dscl . list /Users
dscl . read /Users/llauren
dscl /LDAPv3/ldap.foo.com read /Users/llauren
dscl /Search read /Users/llauren
dscl . read /Users/llauren home
dscl . read /Users/llauren OriginalNodeName
dscl . read /Groups/admin
```

# dseditgroup

group record manipulation tool

```
dseditgroup -o checkmember -m llauren admin
sudo dseditgroup -o edit -a llauren -t user admin
sudo dseditgroup -o edit -a everyone -t group lpadmin
```

# system_profiler

```
system_profiler -listDataTypes
system_profiler SPHardwareDataType
system_profiler SPNetworkDataType
system_profiler SPDisplaysDataType
```

# log (sierra)

is ... _very different_ (not _broken_ per se...)

Daily affirmation: _It's not you, it's Apple_

Run Console as admin or you fail. Also, you get a bunch of `<private>` entries...

Logs are now in `/var/db/diagnostics`. `syslog` ... _works_ but it's looking at the old files.

`sudo log stream --level debug` (or some other level) (for `syslog -w`)

```
log stream --process Keynote
sudo log stream --process cprefs-something #combine yours and root's
log stream --predicate 'eventMessage contains "broken"'
```

You can't scroll back in time using Console :/

```
sudo log collect --output ~/Desktop --start "$(date -v -2H +'something')"
log show ~/Desktop/... --predicate '...'

sudo log config --subsystem com.apple.something ...
```

This is Sierra only. Swift support coming any year now. Eventually support in 10.11. Workaround: XCode 8.

`xcrun simctl spawn booted log show ~/Desktop/...`

# open

```
open some_file
open proto://url
open . # finder window
open foo.xcodeproj
```

# find

# and where's all the cool stuff?
```
find /System/Library/CoreServices -type f -perm +111
/bin
/sbin
/usr/bin
/usr/sbin
/usr/libexec
/System/Library/CoreServices
```

# textutil

ghetto pandoc!

`textutil -convert txt foo.rft`

# afconvert

Audio file convert!

`/usr/bin/afconvert -f AIFF -d I8 sound.caf`

# say / afplay

With afplay you can play -t 1 time one second from head

# what can _you_ do?
