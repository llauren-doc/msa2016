`Day 2 Session 2`

# Full Circle - Charles Edge

* Consumerism is the new buzzword. _Mobilization_ might be a better word.
* Remember taking the iPad through border security?
* Read blog post mikeymikey blogs here, [mMDMacOS](http://michaellynn.github.io/2016/10/04/mDMacOS/)

## Why Mobilization? It's The Future!

* for your environment (which kind?)
* and your career

Remember the I'm a PC guy? Charles met him on the flight to Sweden. _"You're just not Enterprise enough."_  iOS isn't Enterprise Enough because you don't have Anti-virus on it.

# Binding and tools

* Onelogin
* Okta.com
* Apple Enterprise connect
* NoMAD
* MDM
    - push binds
    - push 802.1x
    - custom defalts domain keys
    - login/startup/scripts
    - lack of kerberos thingies

# Is imaging dead?

SIP2? Rich loves APFS.

* Imaging is _mostly dead_
* Thin Imaging
* Still, some must put a fully functional machine on the user's desk
* But you can't image an iOS device enrolled in MDM, so that can be annoying
* NetInstall is still on the Server

## Device based VPP + DEP
...is the closest equivalent to imaging these days

## Scripting All The Things

- ...is being folowed by Swift
- and it's getting more mature

* APIs are more important than ever
    - like the MDM API
    - you learned how to think programatically

## Packages?

Can they disappear? You might not need pre/post scripts on .app bundles. That's kinda technical but supposedly neat. All apps can be self-contained `.app`s.

# Scenarios!

* A school lab where machines need to get reset each morning
    - image?
    - device based VPP + DEP
* A school where students need their profiles/desktops moving with them
    - caching?
* A startup with technical staff that uses mostly web apps
    - MDM only
    - _Bushel_
* Audit CIS or specific regulatory security requirements
    - SIP2? where /System is effectively read only

## MDM

If you now one, you more or less know them all
