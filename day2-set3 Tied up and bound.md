`Day 2 Session 3`

# Tied up and bound - Joel Rennich

_Things are different than they were 10 years ago (sorry!)_

* What can AD do?
* NoMAD ("No More AD")

# AD Functions

* LDAP
* Kerberos
* Lots and lots of data
* and bunches of things we don't need...
    - Group policies, ie, are irrelevant to the Mac

* Single sign on
    - web sites
    - file shares
    - certificate provisioning
    - Exchange
    - DFS
    - printers....

* The Login window?
    - AD plugin adds unnecessary complexity
    - users annoed having to change password every 90 days
    - it's behind the vpn...
    - lots of cals to the service desk
    - and frustration

* Most macs are single user systems
* Macs are often off the network
* Policies come via MDM, not DS (schema extensions)
* No Groups needed (on a single user system)

Is the AD plugin really needed?

Solutions
* AD plugin
* ADPassMon
* Enterprise connect
* KerbMinder (!)
* ShareMounter

...and most of these aren't really what you need

# NoMAD

* Eveything you like about AD without the bind
* SSO, password expiration warnings, password changes, local password sync, Windows CA certificates
* Manageable via profiles/MDM
* Open sourced
* Fairly easy and automagic to set up

`http://www.gitlab.com/mactroll/nomad`
